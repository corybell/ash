/* eslint no-console: 0 */
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import { getEtsyListings } from './api';
import { sign, verify } from './auth'
const port = process.env.PORT || 3000;

const app = express();

app.use(express.static(__dirname));
app.use(bodyParser.json())

app.post('/api/auth', (req, res) => {
    const { password } = req.body
    const token = password === 'banana' ? sign({}) : null
    res.json({ token });
});

app.get('/api/auth', (req, res) => {
    const token = verify(req.headers.authorization)
    if (token) {
        res.json({ token })
        return;
    }
    res.status(403).json({})
})

app.get('/api/listings', getEtsyListings);

app.use('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../build/index.html'));
});

app.listen(port, (err) => {
    console.info(`==> Listening on port ${port}`);
});
