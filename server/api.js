import fetch from 'node-fetch';

const host = 'https://openapi.etsy.com/v2';
const shop = 'AshleyHayDesign';
const apiParam = '?api_key=ru2r37vs0870k5twhfmf0esa';
const limit = '&limit=100';
const includes = '&includes=MainImage(url_75x75,url_170x135,url_570xN)';
const fields = '&fields=listing_id,title,price,quantity,views,num_favorers,category_path,url';
const endpoint = `${host}/shops/${shop}/listings/active${apiParam}${limit}${includes}${fields}`;

const blackList = [451585072, 451592916, 465076803, 240363992, 465062727];

export function getEtsyListings (req, res) {
    fetch(endpoint)
        .then(r => r.json())
        .then(json => {
            // this could potentially move to client if we do sorting anyway
            const results = json.results.filter(r => !blackList.includes(r.listing_id))
            res.json({ results });
        });
}
