const fs   = require('fs');
const jwt   = require('jsonwebtoken');
const path = require('path')

const privateKEY  = fs.readFileSync(path.join(__dirname, './private.key'), 'utf8');
const publicKEY  = fs.readFileSync(path.join(__dirname, './public.key'), 'utf8');  

const signOptions = {
  issuer: "authorization/token",
  subject: "boo@heyashley.com", 
  audience: "heyashley.com",
  expiresIn:  "1d",
  algorithm:  "RS256"   
}
const verifyOptions = {
  issuer: "authorization/token",
  subject: "boo@heyashley.com", 
  audience: "heyashley.com",
  expiresIn:  "1d",
  algorithm:  ["RS256"]
}

export const sign = (payload) => {
  return jwt.sign(payload, privateKEY, signOptions);
};

export const verify = (token) => {
   try {
     return jwt.verify(token, publicKEY, verifyOptions);
   } catch (err){
     return false;
   }
};

export const decode = (token) => {
    return jwt.decode(token, {complete: true});
    //returns null if token is invalid
};
