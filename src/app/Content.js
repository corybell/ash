import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom'
import Home from 'home';
import Shop from 'shop';
import Portfolio from 'portfolio';
import PortfolioDetail from 'portfolio-detail';
import Login from 'login'
import ProtectedRoute from './ProtectedRoute'

const Content = ({ location }) => (
    <div className={`max-height${location === '/' ? '' : ' content'}` }>
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/shop" component={Shop}/>
            <ProtectedRoute path="/portfolio/:case" component={PortfolioDetail}/>                        
            <Route path="/portfolio" component={Portfolio}/>
            <Route path="/login" component={Login}/>
            <Route component={NoMatch}/>
        </Switch>
    </div>
);

Content.propTypes = {
    location: PropTypes.string.isRequired
};
  
export default Content;

const NoMatch = () => (
    <div className="">
      <h3>These are not the droids you are looking for...</h3>
    </div>
);
