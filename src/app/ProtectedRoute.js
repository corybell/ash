import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { Route, Redirect } from 'react-router-dom'
import { withCookies, Cookies } from 'react-cookie';

class PrivateRoute extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      isFetching: true,
      isSuccess: null,
    };
  }

  hasAuth = async (cookies) => {
    if (!window.location.pathname.endsWith('bestbuy')) {
      return true;
    }
    
    const authToken = cookies.get('token')
    if (!authToken) {
      return false;
    }
  
    const config = { headers: { authorization: authToken } }
    const res = await axios
      .get('/api/auth', config)
      .catch(err => {})
  
    if (!res) {
      return false;
    }
  
    const { token } = res.data
    if (!token) {
      return false;
    }
  
    return true;
  }

  componentDidMount = async () => {
    const isSuccess = await this.hasAuth(this.props.cookies)
    this.setState({
      isSuccess,
      isFetching: false
    })
  }

  render() {
    const { isFetching, isSuccess } = this.state;
    const { component: Component, location, ...rest } = this.props;   
    return (
      <Route {...rest} render={rest => {
        if (isFetching) {
          return null;
        }
         
        const success = (
          <div>
            <Component {...rest}/>
          </div>
        );

        const error = (
          <Redirect to={{
            pathname: '/login',
            state: { from: location }
          }}/>
        );

        return isSuccess ? success : error;
      }}/>
    )
  } 
}

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  location: PropTypes.object,
  cookies: PropTypes.instanceOf(Cookies).isRequired
}

export default withCookies(PrivateRoute)