import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';

class Location extends React.Component {
    componentWillReceiveProps(nextProps) {
        if (this.props.location.pathname !== nextProps.location.pathname) {
            this.props.locationChanged(nextProps.location.pathname);
        }
    }
    render() {
        return null;
    }
}
  
Location.propTypes =  {
    locationChanged: PropTypes.func.isRequired,
    location: PropTypes.object.isRequired,
}

export default withRouter(Location);
  