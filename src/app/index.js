import './App.css';
import React from 'react';
import { instanceOf } from 'prop-types';
import { BrowserRouter as Router } from 'react-router-dom';
import { withCookies, Cookies } from 'react-cookie';

import Location from './Location';
import Nav from 'nav';
import Content from './Content';
import Footer from 'footer';

class App extends React.Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    }

    constructor(props) {
        super(props);
        this.state = {
            location: window.location.pathname
        };
        
        this.setLocation = this.setLocation.bind(this);
    }

    render() {
        if (this.state.location === '/') {
            document.getElementById('root').classList.add('max-height');
        } else {
            document.getElementById('root').classList.remove('max-height');
        }

        return (
            <div id="app" className="max-height">
                <Router>
                    <div className="max-height">
                        <Location locationChanged={this.setLocation} />
                        <Nav location={this.state.location}/>
                        <Content location={this.state.location} />
                        <Footer location={this.state.location} />
                    </div>
                </Router>
            </div>
        );
    }

    setLocation(location) {
        this.setState({ location });
    }
}

export default withCookies(App);
