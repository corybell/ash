import './nav.css';
import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { scroller } from 'react-scroll';

import data from 'data/portfolio.json';
import logoBlack from 'media/logo/logo-black.svg';

const Nav = ({ location }) => {
    if (location === '/' || location === '/login') {
        return null;
    }

    let settings, phrase;
    if (location === '/shop') {
        settings = routeMap.shop;
    } else if (location === '/portfolio') {
        settings = routeMap.portfolio;
    } else {
        settings = routeMap.portfolioDetail;
        const split = location.split('/');
        const caseName = split[split.length - 1];
        phrase = data.casesByName[caseName].phrase;
    }
    
    return (
        <div className="container">
            <div className="row header">
                <nav>
                    <ul>
                        <li className="u-pull-left">
                            <Link to={settings.link}>
                                <img className="u-max-full-width" src={logoBlack} alt="ash" />
                            </Link>
                        </li>
                        <li className="u-pull-right">
                            {settings.text(phrase)}
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    );
};

Nav.propTypes = {
    location: PropTypes.string.isRequired,
};

const routeMap = {
    portfolio: { 
        text: () => (<a onClick={click}><h5>{"I'M ASHLEY, A VISUAL DESIGNER"}</h5></a>), 
        link: '/' 
    },
    shop: { 
        text: () => (<h5>{'PINS, PATCHES, AND PETS'}</h5>), 
        link: '/' 
    },
    portfolioDetail: { 
        text: (phrase) => (<h5>{phrase}</h5>), 
        link: '/portfolio' 
    }
};

const click = () => {
    scroller.scrollTo('about-me', { smooth: true, offset: -20 });
};

export default Nav;
