import './Home.css';
import React from 'react';
import { Link } from 'react-router-dom';

const Home = () => (
    <div className="home-container">
        <div className="split top">
            <div className="jumbo">
                <Link to="/portfolio">PORTFOLIO</Link>
            </div>
        </div>
        <div className="split bottom">
            <div className="jumbo">
                <Link to="/shop">SHOP</Link>
            </div>
        </div>
        <div className="background">
        </div>
    </div>
);

export default Home;