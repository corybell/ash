import React from 'react';
import PropTypes from 'prop-types';

const Gig = ({ job }) => (
        <div className={`row banner ${job.color}`}>
            <div className={`container `}>
                <div className="columns seven">
                    <div className="row"><h4>{job.title}</h4></div>
                        <div key={`desc`} className="row">
                            <p className="banner-text">{job.description}</p>
                        </div>
                </div>
                <div className="columns five">
                    <div className="row"><h6>ROLE</h6></div>
                    <div className="row"><p className="banner-text">{job.role}</p></div>
                    <div className="row"><h6>DELIVERABLES</h6></div>
                    <div className="row"><p className="banner-text">{job.deliverables}</p></div>
                </div>
            </div>
        </div>
);

Gig.propTypes = {
    job: PropTypes.object.isRequired
}

export default Gig;
