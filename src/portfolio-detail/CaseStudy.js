import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { scroller } from 'react-scroll';

import imgReg from './client-img-registry';
import Gig from './Gig';
import ImageRow from './ImageRow';

const CaseStudy = (props) => {
    const { caseStudy, next, prev } = props;
    const getSrc = (c) => (imgReg[caseStudy.name][c]);

    return (
        <div >
            { caseStudy.jobs.map((job, i) => 
                <div key={`gig-container-${i}`}>
                    <Gig key={`gig-${i}`} job={job} />
                    {
                        job.images.map((row, j) =>
                            <ImageRow key={`row-${i}-${j}`} row={row} getSrc={getSrc}/>)
                    }
                </div>)
            }
            <div className="container">
                <div className="row next-prev">
                    <div className="columns twelve">
                        <Link className="u-pull-left" to={`/portfolio/${prev}`} onClick={click}><h6>‹ PREVIOUS GOODNESS</h6></Link>
                        <Link className="u-pull-right" to={`/portfolio/${next}`} onClick={click}><h6>NEXT MASTERPIECE ›</h6></Link>
                    </div>
                </div>
            </div>   
        </div>
    );
};

CaseStudy.propTypes = {
    caseStudy: PropTypes.object.isRequired,
    next: PropTypes.string.isRequired,
    prev: PropTypes.string.isRequired,
};

const click = () => {
    scroller.scrollToTop();
}

export default CaseStudy;
