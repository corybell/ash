import './portfolio-detail.css';
import React from 'react';
import PropTypes from 'prop-types';
import data from 'data/portfolio.json';
import CaseStudy from './CaseStudy';

const PortfolioDetail = (props) => {
    const caseName = props.match.params.case;
    const caseStudy = data.casesByName[caseName];
    const i = data.cases.indexOf(caseName);
    
    const n = i < data.cases.length - 1 ? i + 1 : 0;
    const next = data.cases[n];

    const p = i === 0 ? data.cases.length - 1 : i - 1;
    const prev = data.cases[p];

    return(
        <div className="case-study-detail-root">
            <CaseStudy caseStudy={caseStudy} next={next} prev={prev} />
        </div>
    );
};

PortfolioDetail.propTypes = {
    match: PropTypes.object.isRequired
}

export default PortfolioDetail;
