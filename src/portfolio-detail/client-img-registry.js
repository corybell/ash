import m1 from 'media/clients/3m/1.png';
import m2 from 'media/clients/3m/2.png';
import m3 from 'media/clients/3m/3.png';
import m4 from 'media/clients/3m/4.png';
import m5 from 'media/clients/3m/5.png';

import b1 from 'media/clients/bissell/1.png';
import b2 from 'media/clients/bissell/2.png';
import b3 from 'media/clients/bissell/3.png';
import b4 from 'media/clients/bissell/4.png';

import c1 from 'media/clients/chs/1.png';
import c2 from 'media/clients/chs/2.png';
import c3 from 'media/clients/chs/3.png';
import c4 from 'media/clients/chs/4.png';

import n1 from 'media/clients/nerdblock/1.jpg';
import n2 from 'media/clients/nerdblock/2.jpg';
import n3 from 'media/clients/nerdblock/3.jpg';
import n4 from 'media/clients/nerdblock/4.jpg';
import n5 from 'media/clients/nerdblock/5.png';
import n6 from 'media/clients/nerdblock/6.png';

import sh1 from 'media/clients/shout/1.png';
import sh2 from 'media/clients/shout/2.png';

import s1 from 'media/clients/starkey/1.png';
import s2 from 'media/clients/starkey/2.png';
import s3 from 'media/clients/starkey/3.png';
import s4 from 'media/clients/starkey/4.png';
import s5 from 'media/clients/starkey/5.png';

import w1 from 'media/clients/we/1.png';
import w2 from 'media/clients/we/2.png';
import w3 from 'media/clients/we/3.png';
import w4 from 'media/clients/we/4.png';
import w5 from 'media/clients/we/5.png';

import z1 from 'media/clients/zen/1.jpg';
import z2 from 'media/clients/zen/2.jpg';
import z3 from 'media/clients/zen/3.jpg';
import z4 from 'media/clients/zen/4.jpg';

import hb1 from 'media/clients/horizontal/booth/1.jpg';
import hb2 from 'media/clients/horizontal/booth/2.jpg';
import hb3 from 'media/clients/horizontal/booth/3.jpg';
import hb4 from 'media/clients/horizontal/booth/4.jpg';

import hm1 from 'media/clients/horizontal/marketing/1.jpg';
import hm2 from 'media/clients/horizontal/marketing/2.jpg';
import hm3 from 'media/clients/horizontal/marketing/3.jpg';
import hm4 from 'media/clients/horizontal/marketing/4.jpg';
import hm6 from 'media/clients/horizontal/marketing/6.jpg';
import hm7 from 'media/clients/horizontal/marketing/7.jpg';

import bby1 from 'media/clients/bestbuy/1.png';
import bby2 from 'media/clients/bestbuy/2.png'
import bby3 from 'media/clients/bestbuy/3.png'
import bby4 from 'media/clients/bestbuy/4.png'
import bby5 from 'media/clients/bestbuy/5.png'

import mb1 from 'media/clients/medibio/1.png';
import mb2 from 'media/clients/medibio/2.png'
import mb3 from 'media/clients/medibio/3.png'
import mb4 from 'media/clients/medibio/4.png'
import mb5 from 'media/clients/medibio/5.png'
import mb6 from 'media/clients/medibio/6.png'

export default {
    horizontal: {
        'booth/1.jpg': hb1,
        'booth/2.jpg': hb2,
        'booth/3.jpg': hb3,
        'booth/4.jpg': hb4,
        'marketing/1.jpg': hm1,
        'marketing/2.jpg': hm2,
        'marketing/3.jpg': hm3,
        'marketing/4.jpg': hm4,
        'marketing/6.jpg': hm6,
        'marketing/7.jpg': hm7,
        
    },
    '3m':{
        '1.png': m1,
        '2.png': m2,
        '3.png': m3,
        '4.png': m4,
        '5.png': m5,
    },
    bissell: {
        '1.png': b1,
        '2.png': b2,
        '3.png': b3,
        '4.png': b4
    },
    chs: {
        '1.png': c1,
        '2.png': c2,
        '3.png': c3,
        '4.png': c4
    },
    nerdblock: {
        '1.jpg': n1,
        '2.jpg': n2,
        '3.jpg': n3,
        '4.jpg': n4,
        '5.png': n5,
        '6.png': n6
    },
    shout: {
        '1.jpg': sh1,
        '2.jpg': sh2
    },
    starkey: {
        '1.png': s1,
        '2.png': s2,
        '3.png': s3,
        '4.png': s4,
        '5.png': s5
    },
    we: {
        '1.png': w1,
        '2.png': w2,
        '3.png': w3,
        '4.png': w4,
        '5.png': w5
    },
    zen: {
        '1.jpg': z1,
        '2.jpg': z2,
        '3.jpg': z3,
        '4.jpg': z4
    },
    bestbuy: {
        '1.png': bby1,
        '2.png': bby2,
        '3.png': bby3,
        '4.png': bby4,
        '5.png': bby5,
    },
    medibio: {
        '1.png': mb1,
        '2.png': mb2,
        '3.png': mb3,
        '4.png': mb4,
        '5.png': mb5,
        '6.png': mb6
    }
};
