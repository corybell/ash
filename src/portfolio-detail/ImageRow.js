import React from 'react';
import PropTypes from 'prop-types';

const ImageRow = ({ row, getSrc }) => {
    const numCols = row.length;
    return <div className="container">
        <div className="row image-row" >
            { row.map((c, k) => <ImageColumn key={`col-${k}`} className={classMap[numCols]} src={getSrc(c)} />) }
        </div>
    </div>;
};

ImageRow.propTypes = {
    row: PropTypes.array.isRequired,
    getSrc: PropTypes.func.isRequired
};
export default ImageRow;

const classMap = {
    1: 'twelve columns',
    2: 'six columns',
    3: 'four columns'
};

const ImageColumn = ({ className, src }) => (
    <div className={className}><img className="u-max-full-width" src={src} alt="woops" /></div>
);

ImageColumn.propTypes = {
    className: PropTypes.string.isRequired,
    src: PropTypes.string.isRequired
};
