import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { scroller } from 'react-scroll';
import axios from 'axios'
import monster from 'media/monster/1.svg';
import './login.css';
import { withCookies, Cookies } from 'react-cookie';

class Login extends React.Component {
    state = {
        password: '',
        invalid: false
    }
    
    handlePasswordChange = (e) => {
        const password = e.target.value;
        this.setState({ password });
    }

    handleSubmit = (e) => {
        const { password } = this.state
        const { cookies } = this.props

        axios.post('/api/auth', { password }).then(res => {
            const { token } = res.data
            if (!token) {
                this.setState({ invalid: true })
                return
            }
            this.setState({ invalid: false }, () => {
                cookies.set('token', token);
                window.location.href = '/portfolio/bestbuy'
            })
        }).catch(err => {
            this.setState({ invalid: true })
        })
    }
    
    render = () => {
        return (
            // eslint-disable-next-line
            <form action="javascript:void(0);" className="container password-container">
                <div className="row">
                    <div className="columns twelve password-monster-container">
                        <img
                            className="u-max-full-width password-monster"
                            src={monster}
                            alt="hover"
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="columns twelve">
                        <div className="password-blurb">{'This content is password protected.'}</div>
                        <div className="password-blurb">
                            {'If you do not have the password, contact me at boo@heyashley.com.'}
                        </div>
                    </div>
                </div>
                <div className="row margin-top">
                    <div className="columns offset-by-four four">
                        <input
                            value={this.state.password}
                            onChange={this.handlePasswordChange}
                            className="u-full-width"
                            type="password"
                            placeholder="PASSWORD"
                        />
                    </div>
                    <div className="columns four"></div>
                </div>
                <div className="row">
                    <div className="password-incorrect-container columns twelve">
                        { this.state.invalid &&
                            <div className="password-incorrect-message">please try again</div>
                        }
                    </div>
                </div>
                <div className="row ">
                    <div className="columns offset-by-four four">
                        <div className="row padding-top">
                            <div className="columns six" style={{ paddingTop: '3px' }}>
                                <Link
                                    className=""
                                    to={`/portfolio`}
                                    onClick={click}
                                >
                                    <h6>{'‹ back'}</h6>
                                </Link>
                            </div>
                            <div className="columns six">
                                <button
                                    type="submit"
                                    onClick={this.handleSubmit}
                                    className="button-link u-pull-right"
                                >
                                    {'SUBMIT ›'}
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="columns four"></div>
                </div>
                
            </form>
        )
    };
}

Login.propTypes = {
    cookies: PropTypes.instanceOf(Cookies).isRequired
};

export default withCookies(Login);

const click = () => {
  scroller.scrollToTop();
}