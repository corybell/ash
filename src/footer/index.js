import './Footer.css';
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { scroller } from 'react-scroll';

const Footer = ({ location }) => {
    if (location === '/' || location === '/login') {
        return null;
    }

    const settings = location === '/shop' ? routeMap.shop : routeMap.portfolio;
    
    return (
        <footer className="footer">
            <p>
                <Link to={settings.link} onClick={click}>{`VISIT THE ${settings.text}`}</Link>
            </p>
        </footer>
    );
}

Footer.propTypes = {
    location: PropTypes.string.isRequired,
};

const click = () => {
    scroller.scrollToTop();
}

const routeMap = {
    portfolio: { link: '/shop', text: 'SHOP' },
    shop: { link: '/portfolio', text: 'PORTFOLIO' }
};

export default Footer;
  