import React from 'react';
import Media from 'react-media';

import CaseStudy from './CaseStudy';
import data from 'data/portfolio.json';

import mmm from 'media/clients/3m/main.png';
import bissell from 'media/clients/bissell/main.png';
import chs from 'media/clients/chs/main.png';
import horizontal from 'media/clients/horizontal/main.png';
import nerdblock from 'media/clients/nerdblock/main.png';
import shout from 'media/clients/shout/main.png';
import starkey from 'media/clients/starkey/main.png';
import we from 'media/clients/we/main.png';
import zen from 'media/clients/zen/main.png';
import bestbuy from 'media/clients/bestbuy/main.png';
import medibio from 'media/clients/medibio/main.png';

const imageMap = {
    '3m': mmm,
    bissell, chs, horizontal, nerdblock, shout, starkey, we, zen, bestbuy, medibio
};

const CaseStudies = () => (
    <div>
        <Media query="(max-width: 900px)">
            { matches => matches ? renderContent(1) : null }
        </Media>
        <Media query="(min-width:900px) and (max-width: 1199px)">
            { matches => matches ? renderContent(2) : null }
        </Media>
        <Media query="(min-width:1200px)">
            { matches => matches ? renderContent(3) : null }
        </Media>
    </div>
);

const renderContent = (chunkSize) => {
    const grouped = data.cases.reduce((results, caseName) => {
        const caseData = data.casesByName[caseName];
        results.push(caseData);
        return results;
    }, []);
    
    const chunked = chunkResults(grouped, chunkSize);
    return chunked.map((row, i) => {
        return (<div key={`row${i}`} className="row">
            { row.map((caseStudy, j) => 
                <CaseStudy key={j} caseStudy={caseStudy} src={imageMap[caseStudy.name]}
                     chunkSize={chunkSize} />) }
        </div>);
    });
}

export default CaseStudies;

const chunkResults = (results, chunkSize) => {
    let index, newResults = [];
    
    for (index = 0; index < results.length; index += chunkSize) {
        const chunk = results.slice(index, index + chunkSize);
        newResults.push(chunk);
    }

    return newResults;
};