import React from 'react';
import { Element } from 'react-scroll';
import bioJson from 'data/bio.json';

import bby from 'media/logo/bby.jpg';
import bent from 'media/logo/bent.jpg';
import code from 'media/logo/code.jpg';
import crystal from 'media/logo/crystal.jpg';
import mcad from 'media/logo/mcad.jpg';

import fb from 'media/icon/facebook.svg';
import ig from 'media/icon/instagram.svg';
import li from 'media/icon/linkedin.svg';

import bio from 'media/bio.png';

const Bio = () => (
    <div>
        <div className="row">
            <Element name="about-me"/>
            <div className="columns tweleve no-margin">
                <img src={bio} alt="bio" />
            </div>
        </div>
        <div className="row">
            <div className="columns tweleve">
                <h5 className="about-me">ABOUT ME</h5>
            </div>
        </div>
        <div className="row">
            <div className="columns eight offset-by-two blurb">
                <p>{bioJson.bio}</p>
                <p>Design Inquires | <a className="email-link" href={`mailto:${bioJson.email}`}>{bioJson.email}</a></p>
                <p>{bioJson.skills}</p>
            </div>
        </div>
        <div className="row">
            <div className="columns tweleve">
                <h5 className="about-me">MORE HAPPY CLIENTS</h5>
            </div>
        </div>
        <div className="row">
            <div className="columns tweleve portfolio-logo">
                <img src={mcad} alt="mcad" />
                <img src={bent} alt="bent" />
                <img src={crystal} alt="crystal" />
                <img src={code} alt="code" />
                <img src={bby} alt="bby" />
            </div>
        </div>
        <div className="row">
            <div className="columns tweleve portfolio-social">
                <a href={bioJson.facebook} target="_blank"><img src={fb} alt="fb" /></a>
                <a href={bioJson.linkedin} target="_blank"><img src={li} alt="li" /></a>
                <a href={bioJson.instagram} target="_blank"><img src={ig} alt="ig" /></a>
            </div>
        </div>
    </div>
);

export default Bio;
