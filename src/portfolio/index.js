import './Portfolio.css';
import React from 'react';
import CaseStudies from './CaseStudies';
import Bio from './Bio';

const Portfolio = () => (
    <div>
        <div className="container portfolio-root">
            <CaseStudies />
            <Bio />
        </div>
    </div>
);

export default Portfolio;

