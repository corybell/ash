import React from 'react';
import PropTypes from 'prop-types';

import gander from 'media/buttons/1.svg';
import peek from 'media/buttons/2.svg';
import more from 'media/buttons/3.svg';

class CaseStudy extends React.Component {
    constructor(props) {
        super(props);
        this.state = { active: false };
        this.mouseEnter = this.mouseEnter.bind(this);
        this.mouseLeave = this.mouseLeave.bind(this);
        this.click = this.click.bind(this);
        this.getHoverImg = this.getHoverImg.bind(this);
    }

    render() {
        const { caseStudy, src, chunkSize } = this.props;
        const className = classMap[chunkSize];
        const styles = { backgroundImage: `url(${src})` };

        return (
            <div className={className}>
                <button className="u-max-full-width case-study-button" style={styles}
                    title={caseStudy.title}
                    onClick={this.click}
                    onMouseEnter={this.mouseEnter} 
                    onMouseLeave={this.mouseLeave}>
                    { !this.state.active && caseStudy.title }
                    { this.state.active && <img className="u-max-full-width" src={this.getHoverImg()} alt="hover"/> }
                </button>
            </div>
        );
    }

    click () {
        const { caseStudy } = this.props;
        window.location.href = `/portfolio/${caseStudy.name}`;
    }

    getHoverImg () {
        const i = Math.floor(Math.random() * 3);
        return hoverMap[i];
    }

    mouseEnter (e) {
        this.setState({ active: true });
    }

    mouseLeave (e) {
        this.setState({ active: false });
    }
}

CaseStudy.propTypes = {
    caseStudy: PropTypes.object.isRequired,
    src: PropTypes.string.isRequired,
    chunkSize: PropTypes.number.isRequired
};

export default CaseStudy;

const classMap = {
    4: 'three columns',
    3: 'four columns',
    2: 'six columns',
    1: 'twelve columns'
};

const hoverMap = {
    0: gander,
    1: peek,
    2: more
};