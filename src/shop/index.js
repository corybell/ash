import './Shop.css'
import React from 'react';
import axios from 'axios';
import Listing from './Listing';
import Media from 'react-media';

class Shop extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            loading: true,
            listings: []
        };
        this.renderContent = this.renderContent.bind(this);
    }

    render () {
        const style = { visibility: this.state.loading ? 'visible' : 'hidden' };
        return (
            <div>
                <div className="container shop-root">
                    <h6 style={style}>loading...</h6>
                    <Media query="(max-width: 900px)">
                        { matches => matches ? this.renderContent(2) : null }
                    </Media>
                    <Media query="(min-width:900px) and (max-width: 1200px)">
                        { matches => matches ? this.renderContent(3) : null }
                    </Media>
                    <Media query="(min-width:1200px)">
                        { matches => matches ? this.renderContent(4) : null }
                    </Media>
                </div>
            </div>
        );
    }

    renderContent (chunkSize) {
        const s = chunkResults(this.state.listings, chunkSize);
        return s.map((r, i) => {
            return (<div key={`row${i}`} className="row">
                { r.map((l, j) => <Listing key={j} listing={l} chunkSize={chunkSize} />) }
            </div>);
        });
    }

    componentDidMount () {
        axios.get('/api/listings').then(res => {
            if (!res || !res.data || !res.data.results) {
                return;
            }

            this.setState({ loading: false, listings: res.data.results });
        })
        .catch(error => {
            // console.log(error);
        });
    }
}

export default Shop;

const chunkResults = (results, chunkSize) => {
    let index, newResults = [];
    
    for (index = 0; index < results.length; index += chunkSize) {
        const chunk = results.slice(index, index + chunkSize);
        newResults.push(chunk);
    }

    return newResults;
};
