import React from 'react';
import PropTypes from 'prop-types';
const imagePath ='url_570xN';

class Listing extends React.Component {
    constructor(props) {
        super(props);
        this.state = { };
        this.mouseEnter = this.mouseEnter.bind(this);
        this.mouseLeave = this.mouseLeave.bind(this);
    }

    render() {
        const { listing } = this.props;
        const className = classMap[this.props.chunkSize]
        return (
            <div className={className}>
                <a className="listing-wrapper"
                    href={listing.url}
                    target="_blank" 
                    title={listing.title}
                    onMouseEnter={this.mouseEnter} 
                    onMouseLeave={this.mouseLeave}>
                    <img className="u-max-full-width" src={listing.MainImage[imagePath]} alt="listing"/>
                    <p className="listing-title">{listing.title}</p>
                    <p className="price">${listing.price}</p>  
                </a>
            </div>
        );
    }

    mouseEnter (e) {
        // this.setState({ visibility: 'visible' });
    }

    mouseLeave (e) {
        // this.setState({ visibility: 'hidden' });
    }
}

Listing.propTypes = {
    listing: PropTypes.object.isRequired,
    chunkSize: PropTypes.number.isRequired
};

export default Listing;

const classMap = {
    4: 'three columns',
    3: 'four columns',
    2: 'six columns'
};