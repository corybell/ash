const nodeExternals = require('webpack-node-externals');
const path = require('path');

module.exports = {
    entry: './server/index.js',
    target: 'node',
    output: {
        path: path.join(__dirname, 'build'),
        filename: 'server.js'
    },
    node: {
        __filename: false,
        __dirname: false
    },
    externals: [nodeExternals()]
}
