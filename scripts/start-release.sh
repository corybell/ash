#!/bin/bash 
set -e

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 1
fi
rel="release-$1"
# new branch
git checkout -b $rel development
# bump package
npm version minor --no-git-tag-version
# commit 
git commit -am "created $rel"
# push
git push origin $rel