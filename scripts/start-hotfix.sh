#!/bin/bash 
set -e

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 1
fi
br="hotfix-$1"

git checkout -b $br master

npm version minor --no-git-tag-version

git commit -am "created $br"
