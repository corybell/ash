#!/bin/bash 
set -e

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 1
fi
rel="hotfix-$1"
# merge hotfix into master
git checkout master
git merge --no-ff -m "$rel merged to master" $rel
# tag the release
git tag -a -m "$rel tagged" $1
# merge release into dev
git checkout development
git merge --no-ff -m "$rel merged to development" $rel
# push
git push origin master
git push origin development
git push origin --tags

#git branch -d release-1.1