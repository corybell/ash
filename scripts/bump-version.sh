#!/bin/bash 
set -e

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 1
fi

git checkout development

npm version patch --no-git-tag-version

git commit -am "bumped version patch to $1"

git push origin development