#!/bin/bash 
set -e

sftp root@138.197.218.233 << EOF 
  cd ash
  put -r build tmp
  quit
EOF

ssh root@138.197.218.233 << EOF
  rm -rf ash/build
  mv ash/tmp ash/build
  pm2 restart ash/build/server.js
EOF

exit