const gulp = require('gulp');
const install = require('gulp-install');

const serverFiles = [
    'server/package.json',
    'server/private.key',
    'server/public.key',
]

gulp.task('copy-package', () => {
    return gulp.src(serverFiles)
        .pipe(gulp.dest('build'));
});

gulp.task('modules', ['copy-package'], () => {
    return gulp.src('build/package.json')
        .pipe(install({ production: true }))
});

gulp.task('default', [ 
    'modules', 
]);
        